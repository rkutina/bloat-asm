
import bloat.assembler.Assembler;
import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Main {
    public static void main(String[] args) throws Exception {
        HashMap<String, String> config = new HashMap<>();
        
        //handle the arguments here somehow
        for(String arg : args){
            if(arg.contains("=")){
                String[] splt = arg.split("=");
                if(splt.length == 2){
                    if(config.containsKey(splt[0])){
                        throw new Exception("Argument \'" + splt[0] + "\' was already defined!");
                    }
                    config.put(splt[0], splt[1]);
                }else{
                    throw new Exception("Invalid arg format, Arguments should be in key=value form!");
                }
            }else{
                config.put(arg, null);
            }
        }
        
        new Assembler().assembleByConfig(config);
    }
}
