
import bloat.assembler.Assembler;
import java.util.HashMap;
    
/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Test {
    
    public static void main(String[] args) throws Exception {
        HashMap<String, String> config = new HashMap<>();
        
        //config.put("verbose", null);
        config.put("debug", null);
        
        config.put("curdir", "/home/rkutina/testing");
        config.put("in", "/home/rkutina/testing/test2.asm");
        config.put("out", "/home/rkutina/testing/test2.hex");
        
        
        config.put("m", "/home/rkutina/NetBeansProjects/BloatAVR/dist/BloatAVR.jar");
            
        new Assembler().assembleByConfig(config);
    }
}
