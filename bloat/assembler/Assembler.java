package bloat.assembler;

import bloat.exceptions.AssemblerException;
import bloat.exceptions.*;
import bloat.exporters.IntelHexExporter;
import bloat.generic.*;
import bloat.instrinfo.InstrInfo;
import bloat.linker.Linker;
import bloat.preprocessor.Preproc;
import static bloat.utils.Utils.warn;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The methods in this class encompass functions for the actual top-level
 * assembly. Basically, instruction definitions + references to input streams
 * etc go in, and an outputstream gets written.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Assembler {

    public void assembleByConfig(HashMap<String, String> config) throws AssemblerException, FileNotFoundException, FilePermissionException, IOException, ConfigLoaderException, InstantiationException, ClassNotFoundException, IllegalAccessException, Exception {

        File src = null;

        if (config.containsKey("in")) {
            src = new File(config.get("in"));
        } else {
            throw new AssemblerException("Source file was not defined. Try running the assembler again with in=[your source file] as a parameter.");
        }

        if (!src.exists()) {
            throw new FileNotFoundException("The specified source file doesn't seem to exist! (" + src.getAbsolutePath() + ")");
        }

        if (!src.canRead()) {
            throw new FilePermissionException("Check your file permissions! Cannot read the source file! (" + src.getAbsolutePath() + ")");
        }

        File tgt = null;

        if (config.containsKey("out")) {
            tgt = new File(config.get("out"));
        } else {
            throw new AssemblerException("Target file was not defined. Try running the assembler again with out=[your source file] as a parameter.");
        }

        if (tgt.exists()) {
            warn("Output file will be overwritten", !config.containsKey("nowarn"));
        } else {
            warn("Output file wasn't found, creating it now...", !config.containsKey("nowarn"));
            tgt.createNewFile();
        }
        if (!tgt.canWrite()) {
            throw new FilePermissionException("Check your file permissions! Cannot write the target file! (" + src.getAbsolutePath() + ")");
        }

        File asmjar;

        if (config.containsKey("m")) {
            asmjar = new File(config.get("m"));
        } else {
            throw new AssemblerException("External assembling jar file was not defined. Try running the assembler again with m=[your assembling jar file] as a parameter.");
        }

        if (!asmjar.exists()) {
            throw new FileNotFoundException("The specified machine assembling jar file doesn't seem to exist! (" + src.getAbsolutePath() + ")");
        }

        if (!asmjar.canRead()) {
            throw new FilePermissionException("Check your file permissions! Cannot read the machine assembling jar file! (" + src.getAbsolutePath() + ")");
        }

        ClassLoader loader;
        loader = URLClassLoader.newInstance(
                new URL[]{asmjar.toURI().toURL()},
                this.getClass().getClassLoader()
        );

        ConfigLoader cl = new ConfigLoader(loader.getResourceAsStream("config"));
        String igPath = cl.config.containsKey("infog.adr") ? cl.config.get("infog.adr") : "";
        String ipPath = cl.config.containsKey("iproc.adr") ? cl.config.get("iproc.adr") : "";

        InfoGetter ig = (InfoGetter) loader.loadClass(igPath).newInstance();
        InstructionProcessor ip = (InstructionProcessor) loader.loadClass(ipPath).newInstance();

        this.assemRaw(new FileReader(src), new FileWriter(tgt), ig.getIILoader().iinfos, ip, config);
    }

    public void assemRaw(Reader input, Writer output, ArrayList<InstrInfo> iinfos, InstructionProcessor ip, HashMap<String, String> config) throws AssemblerException {
        Preproc p = new Preproc();
        Linker l = new Linker();

        ArrayList<PreprocessedLine> plines;

        System.out.println(output.toString());
        
        try {
            plines = p.preprocess(input, config);

            HashMap<Integer, PreprocessedLine> linked = l.link(iinfos, plines, config);

            HashMap<Integer, Byte> assembled = ip.process(linked, config);

            IntelHexExporter ihx = new IntelHexExporter(16);

            ihx.export(assembled, output);

        } catch (PreprocessorException | LinkerException | IOException | ConversionException | AssemblerProcException | ExporterException e) {
            throw new AssemblerException(e);
        }
    }

}
