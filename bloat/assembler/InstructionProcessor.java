package bloat.assembler;

import bloat.exceptions.AssemblerProcException;
import bloat.generic.PreprocessedLine;
import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public interface InstructionProcessor {
    public HashMap<Integer, Byte> process(HashMap<Integer, PreprocessedLine> lines, HashMap<String, String> config) throws AssemblerProcException;
}
