package bloat.assembler;

import bloat.exceptions.*;
import bloat.generic.Bignum;
import bloat.generic.PreprocessedLine;
import bloat.instrinfo.InstrInfo;
import bloat.instrinfo.InstrInfoLoader;
import bloat.params.*;
import bloat.utils.VLikeParser;
import java.util.HashMap;

/**
 * An example of a part of an instruction processor. Also useful for extending
 * and building other, more complicated contraptions upon.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class UniversalAsm {

    public final InstrInfoLoader iil;

    public UniversalAsm(InstrInfoLoader iil) {
        this.iil = iil;
    }

    public HashMap<Integer, Byte> assemble(HashMap<Integer, PreprocessedLine> lines, boolean endianess) throws AssemblerProcException {
        HashMap<Integer, Byte> ret = new HashMap<>();

        InstrInfo ii = null;

        for (int p : lines.keySet()) {

            PreprocessedLine pl;

            byte[] asmd = assembleLine(lines.get(p), endianess);

            int cix = p;
            for (byte b : asmd) {
                ret.put(cix++, b);
            }
        }
        return ret;

    }

    /**
     * Simply shoves all the parameters in a binary pattern and out pops the
     * resulting binary. Simple, innit.
     *
     * @param pl Preprocessed line coming in, basically straight out of the
     *           Preproc.
     *
     * @return Returns an array of resulting bytes.
     *
     * @throws AssemblerProcException Thrown when something goes fishy, either
     *                                during the parameter conversion or VLike
     *                                parsing.
     */
    private byte[] assembleLine(PreprocessedLine pl, boolean endianess) throws AssemblerProcException {
        VLikeParser vp = new VLikeParser();

        InstrInfo ii = pl.getIinfo();

        try {
            
            Bignum b = vp.parseBinPattern(ii.bpatform, convertParams(pl.params, ii));
            
            if(endianess){
                b.reverseEndianess();
            }
            
            return b.getByteArray();
        } catch (ConversionException | ParserException e) {
            throw new AssemblerProcException(e);
        }
    }

    /**
     * Converts the given AsmParam parameters (most likely gotten straight outta
     * Preproc) according to the InstrInfo mnemonic pattern to a HashMap linking
     * their identifiers to Bignum values, most likely to be used with the
     * VLikeParser.
     *
     * @param params An array of AsmParams. At this stage, it should already
     *               contain ether BignumParam or IntegerParam classes only.
     * @param ii     InstrInfo to match the mnemonic pattern of.
     *
     * @return Returns a HashMap linking String identifiers to corresponding
     *         Bignum parameters.
     *
     * @throws ConversionException Thrown when parameters seem fishy.
     */
    HashMap<String, Bignum> convertParams(AsmParam[] params, InstrInfo ii) throws ConversionException {
        HashMap<String, Bignum> ret = new HashMap<>();

        String[] names = ii.mnemform.split(" ");

        if (params.length != names.length - 1) {
            throw new ConversionException("Parameter lengths don't match! (p:" + params.length + ", n:" + (names.length - 1) + ")");
        }

        for (int i = 0; i < params.length; i++) {
            AsmParam cp = params[i];

            String name = names[i + 1];

            if (name.startsWith("$")) {
                if (cp instanceof IntegerParam) {
                    ret.put(name, new Bignum((Integer) cp.getValue()));
                    continue;
                } else {
                    throw new ConversionException("at \'" + name + "\': Expected " + IntegerParam.class.getCanonicalName() + ", found " + cp.getClass().getCanonicalName());
                }
            }
            if (name.startsWith("l")) {
                if (cp instanceof IntegerParam) {
                    ret.put(name, new Bignum((Integer) cp.getValue()));
                    continue;
                } else if (cp instanceof BignumParam) {
                    ret.put(name, (Bignum) cp.getValue());
                    continue;
                } else {
                    {
                        throw new ConversionException("at \'" + name + "\': Expected " + IntegerParam.class.getCanonicalName() + ", found " + cp.getClass().getCanonicalName());
                    }
                }
            }

            throw new ConversionException("Param \'" + name + "\' has neither an integer nor bignum prefix.");

        }
        return ret;
    }
}
