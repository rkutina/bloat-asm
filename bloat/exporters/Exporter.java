package bloat.exporters;

import bloat.exceptions.ExporterException;
import java.io.Writer;
import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public interface Exporter {
    public void export(HashMap<Integer, Byte> bytes, Writer ow) throws ExporterException;
}
