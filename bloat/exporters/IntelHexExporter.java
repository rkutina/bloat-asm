package bloat.exporters;

import bloat.exceptions.ExporterException;
import bloat.utils.Utils;
import java.io.*;
import java.util.*;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IntelHexExporter implements Exporter {

    private static final int OFF_LEN = 0;
    private static final int OFF_ADRH = 1;
    private static final int OFF_ADRL = 2;
    private static final int OFF_TYPE = 3;

    private final int chunksize;

    public IntelHexExporter(int chunksize) {
        this.chunksize = chunksize;
    }

    @Override
    public void export(HashMap<Integer, Byte> bytes, Writer ow) throws ExporterException {
        
        Integer[] sortedIxes = new Integer[bytes.size()];
        Arrays.sort(bytes.keySet().toArray(sortedIxes));

        try (PrintWriter pw = new PrintWriter(ow)) {
            for (int n = 0; (n < sortedIxes.length); n += chunksize) {
                
                byte[] wbytes = new byte[chunksize + 5];
                
                int startingIx = sortedIxes[n];
                wbytes[OFF_ADRH] = (byte) ((startingIx >> 8) & 0xff);
                wbytes[OFF_ADRL] = (byte) (startingIx & 0xff);
                wbytes[OFF_TYPE] = 0x00;
                
                int ptr = 4;
                
                for (int i = n; ((i - n) < chunksize) && (i < sortedIxes.length); i++) {
                    
                    int ix = sortedIxes[i];
                    
                    if (ix > (ix & 0xffff)) {
                        throw new ExporterException("This version only supports 16bit addresses. Sorry.");
                    }
                    
                    wbytes[ptr++] = (byte) (0xff & bytes.get(ix));
                }
                
                wbytes[OFF_LEN] = (byte) ((ptr - 4) & 0xff);
                
                int checksum = 0;
                for (int ix = 0; ix < ptr; ix++) {
                    checksum += wbytes[ix];
                }
                
                wbytes[ptr++] = (byte) ((-checksum) & 0xff);
                
                String line = ":";
                for (int ix = 0; ix < ptr; ix++) {
                    line += new Utils().padToLengthMultiple(Integer.toHexString(0xff & wbytes[ix]), 2, '0', true);
                }
                
                pw.println(line.toUpperCase());
                
            }
            
            pw.println(":00000001FF");  //intel hex EOF
        }

    }

}
