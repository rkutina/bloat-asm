package bloat.params;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IntegerParam implements AsmParam {

    private final int val;
    
    public IntegerParam(int val){
        this.val = val;
    }
    
    @Override
    public Object getValue() {
        return val;
    }
    
    @Override
    public String toString(){
        return "0x"+Integer.toHexString(this.val);
    }
    
}
