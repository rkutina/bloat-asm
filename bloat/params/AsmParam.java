package bloat.params;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public interface AsmParam {
    public Object getValue();
}
