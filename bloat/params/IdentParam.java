package bloat.params;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class IdentParam implements AsmParam {

    private String str;
    
    public IdentParam(String str){
        this.str = str;
    }
    
    @Override
    public Object getValue() {
        return str;
    }
    
    @Override
    public String toString(){
        return this.str;
    }
    
}
