package bloat.params;

import bloat.generic.Bignum;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class BignumParam implements AsmParam {

    public Bignum bignum;
    
    public BignumParam(Bignum bignum){
        
    }
    
    @Override
    public Object getValue() {
        return bignum;
    }
    
}
