package bloat.params;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class StringParam implements AsmParam {

    private final String str;
    
    public StringParam(String str){
        this.str = str;
    }
    
    @Override
    public Object getValue() {
        return str;
    }
    
    @Override
    public String toString(){
        return str;
    }
}
