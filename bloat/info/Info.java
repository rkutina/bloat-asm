package bloat.info;

import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Info {

    public final HashMap<String, String> infos;

    public Info() {
        this.infos = new HashMap<>();

        this.infos.put("bloat.defaultasm", null);
        this.infos.put("bloat.version", "0");
    }

}
