package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class LinkerException extends Exception {

    public LinkerException() {
        super();
    }

    public LinkerException(String message) {
        super(message);
    }

    public LinkerException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public LinkerException(Throwable cause) {
        super(cause);
    }
}
