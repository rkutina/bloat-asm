package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ConfigLoaderException extends Exception {

    public ConfigLoaderException() {
        super();
    }

    public ConfigLoaderException(String message) {
        super(message);
    }

    public ConfigLoaderException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ConfigLoaderException(Throwable cause) {
        super(cause);
    }
}
