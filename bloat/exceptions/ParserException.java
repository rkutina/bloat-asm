package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ParserException extends Exception {

    public ParserException() {
        super();
    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ParserException(Throwable cause) {
        super(cause);
    }
}
