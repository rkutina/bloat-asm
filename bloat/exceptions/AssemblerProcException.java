package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class AssemblerProcException extends Exception {

    public AssemblerProcException() {
        super();
    }

    public AssemblerProcException(String message) {
        super(message);
    }

    public AssemblerProcException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public AssemblerProcException(Throwable cause) {
        super(cause);
    }
}
