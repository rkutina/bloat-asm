package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ConversionException extends Exception {

    public ConversionException() {
        super();
    }

    public ConversionException(String message) {
        super(message);
    }

    public ConversionException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ConversionException(Throwable cause) {
        super(cause);
    }
}
