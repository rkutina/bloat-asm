package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class AssemblerException extends Exception {

    public AssemblerException() {
        super();
    }

    public AssemblerException(String message) {
        super(message);
    }

    public AssemblerException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public AssemblerException(Throwable cause) {
        super(cause);
    }
}
