package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class FilePermissionException extends Exception {

    public FilePermissionException() {
        super();
    }

    public FilePermissionException(String message) {
        super(message);
    }

    public FilePermissionException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public FilePermissionException(Throwable cause) {
        super(cause);
    }
}
