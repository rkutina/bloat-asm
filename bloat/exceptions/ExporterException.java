package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class ExporterException extends Exception {

    public ExporterException() {
        super();
    }

    public ExporterException(String message) {
        super(message);
    }

    public ExporterException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ExporterException(Throwable cause) {
        super(cause);
    }
}
