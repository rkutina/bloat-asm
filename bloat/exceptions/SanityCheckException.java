package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class SanityCheckException extends Exception {

    public SanityCheckException() {
        super();
    }

    public SanityCheckException(String message) {
        super(message);
    }

    public SanityCheckException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public SanityCheckException(Throwable cause) {
        super(cause);
    }
}
