package bloat.exceptions;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PreprocessorException extends Exception {

    public PreprocessorException() {
        super();
    }

    public PreprocessorException(String message) {
        super(message);
    }

    public PreprocessorException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public PreprocessorException(Throwable cause) {
        super(cause);
    }
}
