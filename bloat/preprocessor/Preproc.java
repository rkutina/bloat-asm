package bloat.preprocessor;

import bloat.exceptions.ConversionException;
import bloat.exceptions.PreprocessorException;
import bloat.generic.tokenizer.GenericToken;
import bloat.generic.PreprocessedLine;
import bloat.params.*;
import bloat.generic.Bignum;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Preproc {

    /**
     * Preprocessed the assembly file's input stream and pops out an ArrayList
     * of PreprocessedLine, containing all the preprocessed lines.
     *
     * @param ir     InputStream of the preprocessed file.
     * @param config HashMap containing some negligible amount of configuration
     *
     * @return Returns an ArrayList of PreprocessedLines.
     *
     * @throws IOException           Thrown when something fudges up with the
     *                               InputStream.
     * @throws PreprocessorException Thrown when something fishy happens to the
     *                               preprocessor itself.
     * @throws ConversionException   Typically thrown when something fishy
     *                               happens while converting the Literals
     *                               (numeric constants).
     */
    public ArrayList<PreprocessedLine> preprocess(Reader ir, HashMap<String, String> config) throws PreprocessorException, ConversionException, IOException {
        BufferedReader br = new BufferedReader(ir);

        ArrayList<PreprocessedLine> lines = new ArrayList<>();

        PreprocTokenizer pt = new PreprocTokenizer();

        boolean debug = config.containsKey("debug");

        String line;
        while ((line = br.readLine()) != null) {
            pt.clear();
            line = line.replaceAll("([^\\\\];(.+)?)|(^;(.+)?)", "");

            try {
                pt.tokenize(line);
            } catch (Exception e) {
                throw new PreprocessorException(e);
            }

            GenericToken[] toks = pt.getTokens();

            if (toks.length == 0) {
                continue;
            }

            int n = 0;

            String name = "ERR";
            AsmParam[] ap = new AsmParam[toks.length - 1];

            for (GenericToken gt : toks) {
                if (n == 0) {
                    if (toks[0].getTokenID() != PreprocTokenizer.TOK_IDENTIFIER) {
                        throw new PreprocessorException("The first token always has to be an identifier!");
                    }
                    name = toks[0].getSequence();
                } else {
                    AsmParam toadd = null;
                    GenericToken ct = toks[n];
                    switch (ct.getTokenID()) {
                        case PreprocTokenizer.TOK_IDENTIFIER: {
                            if (debug) {
                                System.out.println("\tFound an identifier \'" + ct.getSequence() + "\'");
                            }
                            toadd = new IdentParam(ct.getSequence());
                            break;
                        }
                        case PreprocTokenizer.TOK_STRING: {
                            if (debug) {
                                System.out.println("\tFound a string \'" + ct.getSequence() + "\'");
                            }
                            toadd = new StringParam(ct.getSequence().substring(1, ct.getSequence().length() - 1));
                            break;
                        }
                        case PreprocTokenizer.TOK_LITERAL_2: {
                            if (debug) {
                                System.out.println("\tFound a binary literal \'" + ct.getSequence() + "\'");
                            }
                            Bignum b = Bignum.fromBinString(ct.getSequence());
                            if (b.size() >= 0 && b.size() <= 4) {
                                int r = 0;
                                for (int i = 0; i < b.size(); i++) {
                                    r |= ((b.getByteArray()[i]) & 0xff) << i * 8;
                                }
                                toadd = new IntegerParam(r);
                            } else {
                                toadd = new BignumParam(b);
                            }
                            break;
                        }
                        case PreprocTokenizer.TOK_LITERAL_8: {
                            if (debug) {
                                System.out.println("\tFound an octal literal \'" + ct.getSequence() + "\'");
                            }
                            Bignum b = Bignum.fromOctalString(ct.getSequence());
                            if (b.size() >= 0 && b.size() <= 4) {
                                int r = 0;
                                for (int i = 0; i < b.size(); i++) {
                                    r |= ((b.getByteArray()[i]) & 0xff) << i * 8;
                                }
                                toadd = new IntegerParam(r);
                            } else {
                                toadd = new BignumParam(b);
                            }
                            break;
                        }
                        case PreprocTokenizer.TOK_LITERAL_16: {
                            if (debug) {
                                System.out.println("\tFound a hex literal \'" + ct.getSequence() + "\'");
                            }
                            Bignum b = Bignum.fromHexString(ct.getSequence());
                            if (b.size() >= 0 && b.size() <= 4) {
                                int r = 0;
                                for (int i = 0; i < b.size(); i++) {
                                    r |= ((b.getByteArray()[i]) & 0xff) << i * 8;
                                }
                                toadd = new IntegerParam(r);
                            } else {
                                toadd = new BignumParam(b);
                            }
                            break;
                        }
                        case PreprocTokenizer.TOK_LITERAL_10: {
                            if (debug) {
                                System.out.println("\tFound a decimal literal \'" + ct.getSequence() + "\'");
                            }
                            int i = Integer.parseInt(ct.getSequence());
                            toadd = new IntegerParam(i);
                        }
                    }
                    if (toadd != null) {
                        ap[n - 1] = toadd;
                    }
                }
                n++;
            }

            if ("ERR".equals(name)) {
                throw new PreprocessorException("Something has fucked up again!");
            }

            lines.add(new PreprocessedLine(name, ap));
        }

        return lines;
    }

}
