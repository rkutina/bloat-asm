package bloat.preprocessor;

import bloat.generic.tokenizer.GenericToken;
import bloat.generic.tokenizer.GenericTokenizer;

/**
 * A tokenizer class used to tokenize all the assembly code lines.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public final class PreprocTokenizer extends GenericTokenizer {

    public static final int TOK_NULL = 0;
    public static final int TOK_STRING = 1;
    //public static final int TOK_COMMA = 2;
    public static final int TOK_IDENTIFIER = 4;
    public static final int TOK_LITERAL_2 = 10;
    public static final int TOK_LITERAL_8 = 11;
    public static final int TOK_LITERAL_10 = 12;
    public static final int TOK_LITERAL_16 = 13;

    public PreprocTokenizer() {
        this.addTokenInfo("\"[^\"]+\"", TOK_STRING);
        this.addTokenInfo("([01]+[bB])|(0b[01]+)", TOK_LITERAL_2);
        this.addTokenInfo("(0[0-7]+)|([0-7]+[oO])", TOK_LITERAL_8);
        this.addTokenInfo("([0-9]+)", TOK_LITERAL_10);
        this.addTokenInfo("([0-9a-fA-F]+[hH])|(0x[0-9a-fA-F]+)", TOK_LITERAL_16);
        this.addTokenInfo("[^\\s]+", TOK_IDENTIFIER);   //anything else is gonna be treated as an identifier
        this.addTokenInfo("\\s+", TOK_NULL);
    }

    @Override
    public void addTokenInfo(String regex, int tokenID) {
        super.addTokenInfo(regex + "((\\s+)|(,\\s+)|(\\s+,)|(\\s+,\\s+))", tokenID);
    }

    @Override
    public GenericToken[] getTokens() {
        GenericToken[] got = super.getTokens();
        int nlen = got.length;
        for (GenericToken t : got) {
            if (t.getTokenID() == TOK_NULL) {
                nlen--;
            }
        }
        GenericToken[] ret = new GenericToken[nlen];
        int i = 0;
        for (GenericToken t : got) {
            if (t.getTokenID() != TOK_NULL) {
                //ret[i++] = t;
                ret[i++] = new GenericToken(t.getTokenID(),
                        t.getSequence().trim().replaceAll("^,|,$", "").trim()
                );
            }
        }

        return ret;
    }
}
