package bloat.instrinfo;

import bloat.generic.ConfigLoader;
import java.io.*;
import java.util.*;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public final class InstrInfoLoader {

    public final ArrayList<InstrInfo> iinfos;
    public final boolean endianess;

    public InstrInfoLoader(InputStream is) throws Exception {
        iinfos = new ArrayList<>();

        ConfigLoader cl = new ConfigLoader();
        cl.loadConfig(is, false);

        if (cl.config.containsKey("bigendian")) {
            this.endianess = true;
        } else if (cl.config.containsKey("littleendian")) {
            this.endianess = false;
        } else {
            this.endianess = false;
        }

        this.loadInstrInfos(cl);
    }

    public void loadInstrInfos(InputStream is) throws IOException, Exception {
        ConfigLoader c = new ConfigLoader();
        c.loadConfig(is, true);
        loadInstrInfos(c);
    }

    public void loadInstrInfos(ConfigLoader c) throws IOException, Exception {

        Set<String> names = c.extractPrefixes().config.keySet();

        for (String name : names) {
            ConfigLoader nc = c.extractPrefixed(name);

            String mnem = "";
            String bpat = "";
            String slen = "";

            for (String k : nc.config.keySet()) {
                switch (k) {
                    case "mnem": {
                        mnem = nc.config.get(k);
                        break;
                    }
                    case "bpat": {
                        bpat = nc.config.get(k);
                        break;
                    }
                    case "len": {
                        slen = nc.config.get(k);
                        break;
                    }
                }
            }

            InstrInfo ii = new InstrInfo(mnem, bpat, Integer.parseInt(slen));

            iinfos.add(ii);

        }
    }

}
