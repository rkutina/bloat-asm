package bloat.instrinfo;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class InstrInfo {

    public final String mnemform;
    public final String bpatform;
    private int len = 0;
    
    public InstrInfo(String mnemform, String bpatform, int length) {
        this.mnemform = mnemform;
        this.bpatform = bpatform;
        this.len = length;
    }
    
    public int getLength(){
        return this.len;
    }
    
    @Override
    public String toString(){
        return "m: " + this.mnemform + "\tb: " + this.bpatform + "\tl: " + this.len;
    }
}
