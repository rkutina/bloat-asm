package bloat.utils;

import bloat.generic.PreprocessedLine;
import bloat.instrinfo.InstrInfo;
import bloat.params.*;
import java.util.ArrayList;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Utils {

    public Utils() {
        this(false);
    }

    public Utils(boolean debug) {
        this.debug = debug;
    }

    private final boolean debug;

    public String padToLengthMultiple(String s, int len, char padding, boolean frontorback) {
        while (s.length() % len != 0) {
            if (frontorback) {
                s = padding + s;
            } else {
                s += padding;
            }
        }
        return s;
    }

    public static void warn(String str, boolean warn) {
        if (warn) {
            System.out.println("WARN!: " + str);
        }
    }

    public InstrInfo findMatchingIinfo(ArrayList<InstrInfo> iinfos, boolean identifiersMatchAnything, PreprocessedLine pl) {
        outerloop:
        for (InstrInfo ii : iinfos) {

            if (debug) {
                System.out.println("comparing " + ii.mnemform + " to \'" + pl.instruction + "\'");
            }

            if (ii.mnemform.startsWith(pl.instruction)) {

                if (debug) {
                    System.out.println("Matched!");
                }

                String[] forms = ii.mnemform.split(" ");

                if (debug) {
                    System.out.println("fl:" + forms.length + "\tpl:" + pl.params.length);

                    for (String frm : forms) {
                        System.out.println("f:" + frm);
                    }
                    for (AsmParam ap : pl.params) {
                        System.out.println("p:" + ap);
                    }
                }

                if ((forms.length - 1) != pl.params.length) {
                    continue;
                }

                for (int i = 1; i < forms.length; i++) {
                    if (pl.params[i - 1] instanceof IdentParam) {
                        if (identifiersMatchAnything || pl.params[i - 1].getValue().toString().equals(forms[i])) {
                            continue;
                        } else {
                            continue outerloop;
                        }
                    }
                    if (forms[i].startsWith("$")) {
                        if (pl.params[i - 1] instanceof IntegerParam) {
                            continue;
                        } else {
                            continue outerloop;
                        }
                    }
                    if (forms[i].startsWith("l")) {
                        if ((pl.params[i - 1] instanceof IntegerParam) || (pl.params[i - 1] instanceof BignumParam)) {
                            continue;
                        } else {
                            continue outerloop;
                        }
                    }
                    if (forms[i].startsWith("str")) {
                        if (pl.params[i - 1] instanceof StringParam) {
                            continue;
                        } else {
                            continue outerloop;
                        }
                    }
                    if (!(pl.params[i - 1] instanceof IdentParam)) {
                        continue outerloop;
                    }
                }

                return ii;
            }
        }

        return null;
    }
}
