package bloat.utils;

import bloat.exceptions.ConversionException;
import bloat.exceptions.ParserException;
import bloat.generic.Bignum;
import bloat.generic.tokenizer.GenericToken;
import bloat.generic.tokenizer.GenericTokenizer;
import java.util.HashMap;

/**
 * A class to help one parse Bignums into binary patterns. Relied upon by the
 * original UniversalAsm class.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class VLikeParser {

    /**
     *
     * @param bpattern A binary pattern. All [01]+ would get carried over to the
     *                 resulting bignum, anything else is basically an
     *                 identifier (key of the params hashmap). All identifiers
     *                 must be postfixed by a bit-select operator in either of
     *                 these following forms: identifier[from, to] - this would
     *                 select only bits 'from' to 'to' inclusively; or
     *                 identifier[bitnumber], which would only select one bit
     *                 from the given Bignum. 'From', 'to', and 'bitnumber', are
     *                 all decimal representations of the given bit index. Least
     *                 significant bit has index 0. An identifier can be
     *                 optionally prefixed with '!', making the resulting bit
     *                 selection negated.
     * @param params   A hashmap containing the identifiers (which may occur in
     *                 the binary pattern) and their corresponding Bignums.
     *
     * @return Returns a parsed bignum.
     *
     * @throws ParserException     Thrown most usually when the binary pattern
     *                             has an invalid syntax, or when an identifier
     *                             occurring in the pattern doesn't happen to
     *                             occur in the params hashmap.
     * @throws ConversionException Thrown when a Bignum conversion fails.
     */
    public Bignum parseBinPattern(String bpattern, HashMap<String, Bignum> params) throws ParserException, ConversionException {
        VLikeTokenizer vt = new VLikeTokenizer();

        try {
            vt.tokenize(bpattern.replaceAll("\\s+", ""));
        } catch (Exception e) {
            throw new ParserException(e);
        }

        GenericToken[] toks = vt.getTokens();

        String parsable = "";
        String lastident = "";
        boolean negating = false;

        for (int i = 0; i < toks.length; i++) {
            switch (toks[i].getTokenID()) {
                case VLikeTokenizer.ID_BIN: {
                    parsable += toks[i].getSequence();
                    break;
                }
                case VLikeTokenizer.ID_NEG: {
                    negating ^= true;
                    break;
                }
                case VLikeTokenizer.ID_IDENT: {
                    if (!"".equals(lastident)) {
                        throw new ParserException("The identifier " + lastident + " is missing a postfix bitsel operator ( [from(:to)] )."
                                + "Make sure the following pattern \'" + bpattern
                                + "\' contains all variables with bit select operators postfixed.");
                    }

                    lastident = toks[i].getSequence();

                    if (!params.containsKey(lastident)) {
                        throw new ParserException("The given parameter hashmap didn't contain a key \'" + lastident + "\' required for parsing pattern:\nPattern:\t" + bpattern);
                    }

                    //all's good now, keep going.
                    break;
                }
                case VLikeTokenizer.ID_SEL: {
                    if ("".equals(lastident)) {
                        throw new ParserException("Found a bitsel operator with no identifier prefixing it. That's completely wrong in this version. Fix that.\nPattern:\t" + bpattern);
                    }

                    String ss = toks[i].getSequence();

                    ss = ss.substring(1, ss.length() - 1);    //strip the [ and ]

                    int div = ss.indexOf(':');
                    boolean multi = (div != -1);

                    int pre = Integer.parseInt((multi) ? ss.substring(0, div) : ss);
                    int post = (multi) ? Integer.parseInt(ss.substring(div + 1)) : pre;

                    int from = Math.min(pre, post);
                    int to = Math.max(pre, post);

                    String partial = "";

                    Bignum bn = params.get(lastident);

                    if (negating) {
                        bn = bn.negate();
                    }

                    for (int bit = to; bit >= from; bit--) {
                        partial += bn.getBit(bit) ? '1' : '0';
                    }

                    lastident = ""; //clear the last ident., so exceptions won't get thrown next time
                    negating = false;

                    parsable += partial;

                    break;
                }
                default: {
                    throw new ParserException("If you somehow happen to see this as a user, something has f*cked up really bad.");
                }
            }
        }

        return Bignum.fromBinString(parsable);
    }

    /**
     * A tokenizer to tokenize the VLikeParser syntax.
     */
    public class VLikeTokenizer extends GenericTokenizer {

        public static final int ID_BIN = 0;
        public static final int ID_IDENT = 1;
        public static final int ID_SEL = 2;
        public static final int ID_NEG = 3;

        public VLikeTokenizer() {
            this.addTokenInfo("[01]+", ID_BIN);
            this.addTokenInfo("[^01\\[\\]\\!]+", ID_IDENT);
            this.addTokenInfo("\\[[0-9]+(\\:[0-9]+)?\\]", ID_SEL);
            this.addTokenInfo("\\!", ID_NEG);
        }
    }
}
