package bloat.generic.tokenizer;

import java.util.regex.Pattern;

/**
 * TokenInfo contains the regular expression for matching the tokens, also
 * contains the ID's the matching tokens would be assigned.
 */
public class TokenInfo {
    public final Pattern regex;
    public final int tokenID;

    public TokenInfo(Pattern regex, int tokenID) {
        super();
        this.regex = regex;
        this.tokenID = tokenID;
    }
    
}
