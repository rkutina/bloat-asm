package bloat.generic.tokenizer;

/**
 * GenericToken class. Used for storing token information.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 * @author Originally snipped from http://cogitolearning.co.uk/?p=525 (2015) and
 * modified a little.
 */
public class GenericToken {


    /**
     * A number identifying the given token. This would fairly probably come
     * from the TokenInfo definition.
     */
    private final int tokenID;

    /**
     * A string this token was made from.
     */
    private final String sequence;

    /**
     * Token. Used for storing token sequence and its detected identifier.
     *
     * @param tokenID
     * @param sequence
     */
    public GenericToken(int tokenID, String sequence) {
        this.tokenID = tokenID;
        this.sequence = sequence;
    }

    /**
     * Gets the token ID.
     *
     * @return Returns the token ID.
     */
    public int getTokenID() {
        return this.tokenID;
    }

    /**
     * Gets the string this token was made from.
     *
     * @return sequence
     */
    public String getSequence() {
        return this.sequence;
    }
    
    
    @Override
    public String toString() {
        return this.sequence + " (" + this.tokenID + ")";
    }

}
