package bloat.generic.tokenizer;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A generic tokenizer. Used for tokenization of a given input string.
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 * @author Originally snipped from http://cogitolearning.co.uk/?p=525 (2015) and
 * modified a little.
 */
public abstract class GenericTokenizer {

    public final LinkedList<TokenInfo> tokenInfos;
    private final LinkedList<GenericToken> tokens;

    public GenericTokenizer() {
        tokenInfos = new LinkedList<>();
        tokens = new LinkedList<>();
    }

    public void addTokenInfo(String regex, int tokenID) {
        tokenInfos.add(new TokenInfo(Pattern.compile("^(" + regex + ")"), tokenID));
    }

    public void tokenize(String str) throws Exception {
        String s = str.trim() + " ";
        //tokens.clear();
        while (!s.equals(" ")) {
            boolean match = false;
            for (TokenInfo info : tokenInfos) {
                Matcher m = info.regex.matcher(s);
                if (m.find()) {
                    match = true;
                    String tok = m.group().trim();
                    s = m.replaceFirst("").trim() + " ";
                    tokens.add(new GenericToken(info.tokenID, tok));
                    break;
                }
            }
            if (!match) {
                throw new Exception("Unexpected character in input: " + s);
            }
        }
    }

    public GenericToken[] getTokens() {
        return tokens.toArray(new GenericToken[tokens.size()]);
    }

    public void clear() {
        this.tokens.clear();
    }

}
