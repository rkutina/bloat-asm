package bloat.generic;

import bloat.exceptions.ConfigLoaderException;
import bloat.utils.Utils;
import java.io.*;
import java.util.HashMap;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public final class ConfigLoader {

    public final HashMap<String, String> config;

    public ConfigLoader() {
        config = new HashMap<>();
    }

    public ConfigLoader(InputStream is) throws IOException, ConfigLoaderException {
        this();
        this.loadConfig(is, true);
    }
    
    public ConfigLoader(HashMap<String, String> config) {
        this.config = config;
    }

    public ConfigLoader extractPrefixed(String prefix) {
        HashMap<String, String> ret = new HashMap<>();

        prefix += ".";

        for (String k : config.keySet()) {
            if (k.startsWith(prefix)) {
                ret.put(k.substring(prefix.length()), config.get(k));
            }
        }

        return new ConfigLoader(ret);
    }

    public ConfigLoader extractPrefixes() {
        HashMap<String, String> ret = new HashMap<>();

        for (String k : config.keySet()) {
            if (k.contains(".")) {
                ret.put(k.substring(0, k.indexOf('.')), null);
            }
        }

        return new ConfigLoader(ret);
    }

    public ConfigLoader extractPostfixes(String postfix) {
        HashMap<String, String> ret = new HashMap<>();

        postfix = "." + postfix;

        for (String k : config.keySet()) {
            if (k.endsWith(postfix)) {
                ret.put(k.substring(0, k.length() - postfix.length()), config.get(k));
            }
        }

        return new ConfigLoader(ret);
    }

    public void loadConfig(InputStream is, boolean warn_on_redef) throws IOException, ConfigLoaderException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line = "";
        while ((line = br.readLine()) != null) {
            line = line.replaceAll("(^#.+)|([^\\\\]#.+)", "");  //strips comments
            line = line.trim();

            if (line.length() == 0) {
                continue;   //ignore empty lines
            }

            if (!line.contains("=")) {    //all lines should be in the key=value format
                throw new ConfigLoaderException("Unacceptable config format! All lines should be in the 'key = value' format!");
            }

            int splix = line.indexOf('=');

            String pre = line.substring(0, splix).trim();

            String post = (splix >= line.length() - 1) ? "" : (line.substring(splix + 1)).trim();

            if (this.config.containsKey(pre)) {
                Utils.warn("Redefining key '" + pre + "'", warn_on_redef);
            }

            config.put(pre, post);
        }
    }
}
