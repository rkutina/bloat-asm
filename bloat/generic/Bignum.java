package bloat.generic;

import bloat.exceptions.ConversionException;
import bloat.utils.Utils;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Bignum {

    private byte[] bignum;

    public Bignum(byte[] bignum) {
        this.bignum = bignum;
    }

    public Bignum(int num) {
        this.bignum = new byte[]{
            (byte) (num & 0xff),
            (byte) ((num >> 8) & 0xff),
            (byte) ((num >> 16) & 0xff),
            (byte) ((num >> 24) & 0xff),};
    }

    public Bignum(long num) {
        this.bignum = new byte[]{
            (byte) (num & 0xff),
            (byte) ((num >> 8) & 0xff),
            (byte) ((num >> 16) & 0xff),
            (byte) ((num >> 24) & 0xff),
            (byte) ((num >> 32) & 0xff),
            (byte) ((num >> 40) & 0xff),
            (byte) ((num >> 48) & 0xff),
            (byte) ((num >> 56) & 0xff),};
    }

    public void reverseEndianess() {
        byte[] rev = new byte[this.bignum.length];

        for (int i = 0; i < rev.length; i++) {
            rev[i] = this.bignum[rev.length - i - 1];
        }
        
        this.bignum = rev;
    }

    @Override
    public String toString() {
        if (this.bignum == null) {
            return null;
        }
        String ret = "";
        for (byte b : bignum) {
            ret = new Utils().padToLengthMultiple(Integer.toHexString(b & 0xff), 2, '0', true) + ret;
        }

        ret = new Utils().padToLengthMultiple(ret, 2, '0', true);
        return "(" + this.size() + ") 0x" + ret;
    }

    public byte[] getByteArray() {
        return this.bignum;
    }

    public int size() {
        return bignum.length;
    }

    public boolean getBit(int pos) {
        int arraypos = (pos >> 3);
        int bytepos = (pos % 8);

        if (arraypos >= bignum.length || arraypos < 0) {
            return false;
        }

        return (((bignum[pos >>> 3] & 0xff) >> (pos % 8)) & 1) == 1;
    }

    public void setBit(int pos, boolean val) {
        bignum[pos / 8] = (val ? ((byte) (bignum[pos / 8] | (1 << (pos % 8)))) : ((byte) (bignum[pos / 8] & ~(1 << (pos % 8)))));
    }

    public static Bignum fromHexString(String str) throws ConversionException {
        if (str.matches("0x[0-9a-fA-F]+")) {
            str = str.substring(2);
        } else if (str.matches("[0-9a-fA-F]+[hH]")) {
            str = str.substring(0, str.length() - 1);
        } else if (!str.matches("[0-9a-fA-F]+")) {
            throw new ConversionException("Failed to convert hexadecimal string " + str + " to bignum.");
        }

        str = new Utils().padToLengthMultiple(str, 2, '0', true);

        byte[] bignum = new byte[str.length() / 2];

        for (int i = 0; i < bignum.length; i++) {
            int m = (bignum.length - 1 - i) * 2;
            bignum[i] = (byte) ((byte) Integer.parseInt(str.substring(m, m + 2), 16) & 0xff);
        }

        return new Bignum(bignum);
    }

    public static Bignum fromBinString(String str) throws ConversionException {
        if (str.matches("0b[01]+")) {
            str = str.substring(2);
        } else if (str.matches("[01]+[bB]")) {
            str = str.substring(0, str.length() - 1);
        } else if (!str.matches("[01]+")) {
            throw new ConversionException("Failed to convert binary string \'" + str + "\' to bignum.");
        }

        str = new Utils().padToLengthMultiple(str, 4, '0', true);

        String hexstr = "";

        for (int i = 0; i < str.length(); i += 4) {
            switch (str.substring(i, i + 4)) {
                case "0000":
                    hexstr += '0';
                    break;
                case "0001":
                    hexstr += '1';
                    break;
                case "0010":
                    hexstr += '2';
                    break;
                case "0011":
                    hexstr += '3';
                    break;
                case "0100":
                    hexstr += '4';
                    break;
                case "0101":
                    hexstr += '5';
                    break;
                case "0110":
                    hexstr += '6';
                    break;
                case "0111":
                    hexstr += '7';
                    break;
                case "1000":
                    hexstr += '8';
                    break;
                case "1001":
                    hexstr += '9';
                    break;
                case "1010":
                    hexstr += 'a';
                    break;
                case "1011":
                    hexstr += 'b';
                    break;
                case "1100":
                    hexstr += 'c';
                    break;
                case "1101":
                    hexstr += 'd';
                    break;
                case "1110":
                    hexstr += 'e';
                    break;
                case "1111":
                    hexstr += 'f';
                    break;
                default:
                    throw new ConversionException("The person coding this forgot at least one bit pattern it seems.");
            }
        }

        return fromHexString(hexstr);
    }

    public static Bignum fromOctalString(String str) throws ConversionException {
        if (str.matches("0[0-7]+")) {
            str = str.substring(2);
        } else if (str.matches("[0-7]+[oO]")) {
            str = str.substring(0, str.length() - 1);
        } else if (!str.matches("[07]+")) {
            throw new ConversionException("Failed to convert octal string " + str + " to bignum.");
        }

        str = new Utils().padToLengthMultiple(str, 4, '0', true);

        String binstr = "";

        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case '0':
                    binstr += "000";
                    break;
                case '1':
                    binstr += "001";
                    break;
                case '2':
                    binstr += "010";
                    break;
                case '3':
                    binstr += "011";
                    break;
                case '4':
                    binstr += "100";
                    break;
                case '5':
                    binstr += "101";
                    break;
                case '6':
                    binstr += "110";
                    break;
                case '7':
                    binstr += "111";
                    break;
            }
        }

        return fromBinString(binstr);
    }

    public Bignum negate() {
        byte ba[] = new byte[this.bignum.length];

        for (int i = 0; i < ba.length; i++) {
            ba[i] = (byte) ~this.bignum[i];
        }

        return new Bignum(ba);
    }

    /*
     public static Bignum fromDecimalString(String str) {
     //TODO
     }
     */
}
