package bloat.generic;

import bloat.instrinfo.InstrInfoLoader;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public interface InfoGetter {
    public InstrInfoLoader getIILoader() throws Exception;
    public ConfigLoader getConfigLoader() throws Exception;
}
