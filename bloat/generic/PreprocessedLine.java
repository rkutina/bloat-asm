package bloat.generic;

import bloat.instrinfo.InstrInfo;
import bloat.params.AsmParam;
import java.io.File;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class PreprocessedLine {

    public final File origfile;
    public final int origline;
    
    public final String instruction;
    public final AsmParam[] params;
    
    private InstrInfo iinfo;
    
    private int length;

    public PreprocessedLine(String instruction, AsmParam[] params) {
        this(instruction, params, null, -1);
        this.length = 0;
        this.iinfo = null;
    }
    
    public int getLength(){
        return this.length;
    }
    
    public void setLength(int len){
        this.length = len;
    }
    
    public void setIinfo(InstrInfo ii){
        this.iinfo = ii;
    }
    
    public InstrInfo getIinfo(){
        return this.iinfo;
    }
    
    
    public PreprocessedLine(String instruction, AsmParam[] params, File origfile, int origline) {
        this.instruction = instruction;
        this.params = params;
        this.origfile = origfile;
        this.origline = origline;
    }

    @Override
    public String toString() {
        String ret = instruction;
        for (AsmParam par : params) {
            ret += " " + par.toString();
        }
        return ret;
    }
}
