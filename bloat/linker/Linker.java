package bloat.linker;

import bloat.exceptions.*;
import bloat.generic.PreprocessedLine;
import java.util.*;
import bloat.params.*;
import bloat.preprocessor.Preproc;
import bloat.utils.Utils;
import bloat.instrinfo.InstrInfo;
import java.io.*;

/**
 *
 * @author Richard Kutina <kutinric@fel.cvut.cz>
 */
public class Linker {

    public HashMap<Integer, PreprocessedLine> link(
            ArrayList<InstrInfo> iinfos,
            ArrayList<PreprocessedLine> lines,
            HashMap<String, String> config) throws LinkerException, PreprocessorException, IOException, FileNotFoundException, ConversionException {

        boolean verbose = config.containsKey("verbose");
        boolean debug = config.containsKey("debug");

        boolean warn = !config.containsKey("nowarn");

        HashMap<Integer, PreprocessedLine> res = new HashMap<>();
        HashMap<String, AsmParam> equs = new HashMap<>();

        int current = 0;
        String section = "";

        for (int ix = 0; ix < lines.size(); ix++) {
            PreprocessedLine line = lines.get(ix);
            if (line.instruction.charAt(0) == '.') {
                //it's a directive!
                if (debug) {
                    System.out.println("\tfound a directive " + line.instruction);
                }

                switch (line.instruction) {
                    case ".org": {
                        if (line.params.length == 1 && line.params[0] instanceof IntegerParam) {
                            current = (int) (line.params[0]).getValue();
                        } else {
                            throw new LinkerException("Incorrect params on line " + line);
                        }
                        break;
                    }
                    case ".space": {
                        if (line.params.length == 1 && line.params[0] instanceof IntegerParam) {
                            current += (int) (line.params[0]).getValue();
                        } else {
                            throw new LinkerException("Incorrect params on line " + line);
                        }
                        break;
                    }
                    case ".align": {
                        if (line.params.length == 1 && line.params[0] instanceof IntegerParam) {
                            int alignto = (int) (line.params[0]).getValue();
                            current += ((alignto - current) % alignto);
                        } else {
                            throw new LinkerException("Incorrect params on line " + line);
                        }
                        break;
                    }
                    case ".equ": {
                        if (line.params.length == 1 || line.params.length == 2 && line.params[0] instanceof IdentParam) {
                            AsmParam def = null;
                            if (line.params.length == 2) {
                                def = line.params[1];
                            }

                            equs.put((String) line.params[0].getValue(), def);

                        } else {
                            throw new LinkerException("Incorrect params on line " + line + "\n\n" + paramsToString(line.params));
                        }
                        break;
                    }
                    case ".include": {
                        if (line.params.length == 1 && line.params[0] instanceof StringParam) {
                            Preproc p = new Preproc();

                            String incpath = "";
                            //TODO: replace include search paths with something more sophisticated
                            if (config.containsKey("curdir")) {
                                incpath = config.get("curdir");
                            } else {
                                incpath = System.getProperty("usr.dir");
                            }

                            incpath += "/" + (String) line.params[0].getValue();
                            
                            ArrayList<PreprocessedLine> incl = p.preprocess(new FileReader(incpath), config);

                            lines.addAll(ix + 1, incl);
                        } else {

                        }
                        break;
                    }
                    default: {
                        throw new LinkerException("Found an unknown directive " + line.instruction);
                    }
                }
                continue;
            }
            if (line.instruction.charAt(line.instruction.length() - 1) == ':') {
                //it's a label!
                if (debug) {
                    System.out.println("found a label " + line.instruction);
                }

                equs.put(line.instruction.substring(0, line.instruction.length() - 1), new IntegerParam(current));
                continue;
            }

            //it's something else, find out if it matches.
            if (debug) {
                System.out.println("found an instruction " + line.instruction);
            }

            InstrInfo ii = new Utils().findMatchingIinfo(iinfos, true, line);

            if (ii == null) {
                throw new LinkerException("Matching instruction not found in instruction definitions: " + line);
            } else {
                line.setIinfo(ii);
            }

            res.put(current, line);
            current += ii.getLength();
        }

        //pass in all the equs
        for (int key : res.keySet()) {
            PreprocessedLine l = res.get(key);

            for (int px = 0; px < l.params.length; px++) {
                AsmParam cp = l.params[px];
                if (cp instanceof IdentParam) {
                    String eqk = (String) cp.getValue();
                    if (equs.containsKey(eqk)) {
                        l.params[px] = equs.get(eqk);
                    } else {
                        Utils.warn("Found an undefined label " + eqk, warn);
                    }
                }
            }
        }

        //TODO: handle in-line directives
        if (debug) {
            System.out.println("Linked:");
            Integer[] ks = new Integer[res.size()];
            Arrays.sort(res.keySet().toArray(ks));
            for (int key : ks) {
                System.out.println("\t" + key + "\t" + res.get(key));
            }
        }

        return res;
    }

    private String paramsToString(AsmParam[] params) {
        String ret = "";
        for (AsmParam p : params) {
            ret += p.getClass().getName() + "\t" + p.toString() + "\n";
        }
        return ret;
    }
}
